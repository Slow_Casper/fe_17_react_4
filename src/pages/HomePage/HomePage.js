import React from "react";
import StoreList from "../../Components/StoreList/StoreList";

const Home = () => {

    return (
        <>
            <StoreList />
        </>
    );
};

export default Home;
