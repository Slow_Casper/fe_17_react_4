import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./index.css";
import Header from "./Components/Header/Header";
import AllRoutes from "./Routes";
import { fetchItems } from "./redux/store/actionCreators";
import { saveToLocalStorage } from "./utils/localStorage/localStorage";
import { getFavourite } from "./redux/favourite/actionCreators";
import { getCart } from "./redux/cart/actionCreators";
import Modal from "./Components/Modal/Modal";

function App() {
    const dispatch = useDispatch();
    const { isModalOpen, isRemoveModalOpen, item } = useSelector((state) => state.modal);

    useEffect(() => {
        if (!localStorage.cartList) {
            saveToLocalStorage("cartList", []);
        }
        if (!localStorage.wishList) {
            saveToLocalStorage("wishList", []);
        }
        dispatch(fetchItems());
        dispatch(getFavourite());
        dispatch(getCart());
    }, [dispatch]);

    return (
        <>
            <div className="App">
                <Header />
                <AllRoutes />
            </div>

            {isModalOpen && (
                <Modal text={`Товар "${item.name}" додано до кошика`} />
            )}
            {isRemoveModalOpen && (
                <Modal
                    text={`Ви впевнені, що бажаєте видалити товар ${item.name} з корзини?`}
                />
            )}
        </>
    );
}

export default App;
