import StoreItem from "../../Components/StoreItem/StoreItem"

export const mapArr = (array) => {
    return array.map((el) => {return <StoreItem key={el.id} item={el} />})
}